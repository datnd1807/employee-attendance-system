﻿using Admin_Page.Environment;
using Admin_Page.Filters;
using Admin_Page.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList; // thêm thư viện này


namespace Admin_Page.Controllers
{
    [UserAuthenticationFilter]
    public class StoreController : Controller
    {
        // GET: Store
        dynamic mymodel = new ExpandoObject();
        //TempDataDictionary["page"] = pageSize;
        public async Task<ActionResult> GenerateCode()
        {
                using (var client = new HttpClient())
            {
                var StoreList = new List<StoreViewModel>();
                var data = new BaseModel<IEnumerable<StoreViewModel>>();
                //Passing service base url  
                client.BaseAddress = new Uri(ApiUrl.baseUrl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource get all stores using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(ApiUrl.GetListStores);
                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var StoreResponse = Res.Content.ReadAsStringAsync().Result;
                    
                    //Deserializing the response recieved from web api and storing into the store list  
                    data = JsonConvert.DeserializeObject<BaseModel<IEnumerable<StoreViewModel>>>(StoreResponse);    
                    StoreList = (List<StoreViewModel>)data.Data;
                    
                    //returning the store list to view  
                    TempData["Count"] = StoreList.Count;
                    TempData["myList"] = StoreList;
                    mymodel.Stores = StoreList;
                    mymodel.Codes = null;
                    mymodel.SelectedStoreId = null;
                    return View(mymodel);
                }

                return View();
            }

        }

        private async void GetListStore()
        {
            //return View();
            List<StoreViewModel> StoreList = new List<StoreViewModel>();
            BaseModel<IEnumerable<StoreViewModel>> data = new BaseModel<IEnumerable<StoreViewModel>>();
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(ApiUrl.baseUrl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource get all stores using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(ApiUrl.GetListStores);
                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var StoreResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the store list  
                    data = JsonConvert.DeserializeObject<BaseModel<IEnumerable<StoreViewModel>>>(StoreResponse);
                    StoreList = (List<StoreViewModel>)data.Data;

                    //returning the store list to view  
                    TempData["myList"] = StoreList;
                } 
            }
        }
       private string getName(string id)
        {
            var list = TempData["myList"] as List<StoreViewModel>;
            foreach (var item in list)
            {
                if (item.Id.Equals(id))
                {
                    return item.Name;
                }
            }
            return null;

        }

        public async Task<ActionResult> AuthorizedCode()
        {
            var list = TempData["myList"] as List<StoreViewModel>;
            string storeId = Request.Form["Select_Store"];
            using (var client = new HttpClient())
            {
                var data = new BaseModel<StoreViewModel>();
                StoreViewModel code = new StoreViewModel();
                client.BaseAddress = new Uri(ApiUrl.baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

               HttpResponseMessage Res = await client.GetAsync(ApiUrl.AuthorizeCodeUrl + storeId); //API controller name


                if (Res.IsSuccessStatusCode)
                {
                    var result =  Res.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<BaseModel<StoreViewModel>>(result);
                    code = data.Data;
                    if (code != null)
                    {
                        mymodel.Codes = code;
                        mymodel.Stores = list;
                        mymodel.SelectedStoreId = storeId;
                        TempData["myList"] = list;
                        return View("GenerateCode", mymodel);
                    }
                    
                }
                
                
            }
            mymodel.Stores = list;
            mymodel.SelectedStoreId = storeId;
            mymodel.Codes = null;
            TempData["myList"] = list;
            return View("GenerateCode", mymodel);

        }

        private async Task<List<StoreViewModel>> GetStoreViewsAsync(string sortProperty, string orderBy,string searchString)
        {
            List<StoreViewModel> StoreList = new List<StoreViewModel>();
            BaseModel<IEnumerable<StoreViewModel>> data = new BaseModel<IEnumerable<StoreViewModel>>();
            string search = "";
            if (!String.IsNullOrEmpty(searchString)) search = searchString;
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(ApiUrl.baseUrl);
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource get all stores using HttpClient  
                //getStoresDes/name/desc
                //HttpResponseMessage Res = await client.GetAsync(ApiUrl.GetListStores);
                HttpResponseMessage Res = await client.GetAsync(ApiUrl.GetStores + sortProperty + "/" + orderBy+"/" + search);
                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var StoreResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the store list  
                    data = JsonConvert.DeserializeObject<BaseModel<IEnumerable<StoreViewModel>>>(StoreResponse);
                    StoreList = (List<StoreViewModel>)data.Data;

                    //returning the store list to view  
                    TempData["myList"] = StoreList;
                    return StoreList;
                }
                return StoreList;
            }

        }

        public async Task<ActionResult> Index(int? size, int? page, string sortProperty, string sortOrder, string searchString)
    {
            List<StoreViewModel> StoreList = new List<StoreViewModel>();


        // 1. Tạo biến ViewBag gồm sortOrder, searchValue, sortProperty và page
        if (sortOrder == "asc") ViewBag.sortOrder = "desc";
        if (sortOrder == "desc") ViewBag.sortOrder = "";
        if (sortOrder == "") ViewBag.sortOrder = "asc";
        ViewBag.searchValue = searchString;
        ViewBag.sortProperty = sortProperty;
        ViewBag.page = page;

        // 2. Tạo danh sách chọn số trang
        List<SelectListItem> items = new List<SelectListItem>();
        items.Add(new SelectListItem { Text = "5", Value = "5" });
        items.Add(new SelectListItem { Text = "10", Value = "10" });
        items.Add(new SelectListItem { Text = "20", Value = "20" });


        // 2.1. Thiết lập số trang đang chọn vào danh sách List<SelectListItem> items
        foreach (var item in items)
        {
            if (item.Value == size.ToString()) item.Selected = true;
        }
        ViewBag.size = items;
        ViewBag.currentSize = size;

        // 3. Lấy tất cả tên thuộc tính của lớp Link (LinkID, LinkName, LinkURL,...)
        var properties = typeof(StoreViewModel).GetProperties();
        List<Tuple<string, bool, int>> list = new List<Tuple<string, bool, int>>();
        foreach (var item in properties)
        {
            int order = 999;
            var isVirtual = item.GetAccessors()[0].IsVirtual;

                if (item.Name == "StoreCode") order = 2;
                else if (item.Name == "Id") order = 1;
                else if (item.Name == "Name") order = 3;
                else if (item.Name == "Address") order = 4;
                else if (item.Name == "AuthorizeCode") order = 5;
                else continue;
            if (item.Name == "ShortName") continue; // Không hiển thị cột này
            Tuple<string, bool, int> t = new Tuple<string, bool, int>(item.Name, isVirtual, order);
            list.Add(t);
        }
        list = list.OrderBy(x => x.Item3).ToList();

        // 3.1. Tạo Heading sắp xếp cho các cột
        foreach (var item in list)
        {
            if (!item.Item2)
            {
                if (sortOrder == "desc" && sortProperty == item.Item1)
                {
                        ViewBag.Headings += "<th><a href='?page=" + page + "&size=" + ViewBag.currentSize + "&sortProperty=" + item.Item1 + "&sortOrder=" +
                            ViewBag.sortOrder + "&searchString=" + searchString + "'>" + item.Item1 + "<i class='fa fa-fw fa-sort-desc'></i></th></a></th>";
                        //ViewBag.Headings += "<th>" + item.Item1 + "<i class='fa fa-fw fa-sort-desc'></i></th></a></th>";
                    }
                else if (sortOrder == "asc" && sortProperty == item.Item1)
                {
                        ViewBag.Headings += "<th><a href='?page=" + page + "&size=" + ViewBag.currentSize + "&sortProperty=" + item.Item1 + "&sortOrder=" +
                            ViewBag.sortOrder + "&searchString=" + searchString + "'>" + item.Item1 + "<i class='fa fa-fw fa-sort-asc'></a></th>";
                        //ViewBag.Headings += "<th>" + item.Item1 + "<i class='fa fa-fw fa-sort-asc'></a></th>";
                    }
                else
                {
                        ViewBag.Headings += "<th><a href='?page=" + page + "&size=" + ViewBag.currentSize + "&sortProperty=" + item.Item1 + "&sortOrder=" +
                           ViewBag.sortOrder + "&searchString=" + searchString + "'>" + item.Item1 + "<i class='fa fa-fw fa-sort'></a></th>";
                        //ViewBag.Headings += "<th>" + item.Item1 + "<i class='fa fa-fw fa-sort'></a></th>";
                    }

            }
            else ViewBag.Headings += "<th>" + item.Item1 + "</th>";
        }

            if (String.IsNullOrEmpty(sortProperty)) sortProperty = "Id";
            if (sortOrder == "desc") { StoreList = await GetStoreViewsAsync(sortProperty, "desc", searchString); }
            else if (sortOrder == "asc") { StoreList = await GetStoreViewsAsync(sortProperty, "asc", searchString); }
            else { StoreList = await GetStoreViewsAsync(sortProperty, "asc", searchString); }
         


            // 5.2. Nếu page = null thì đặt lại là 1.
            page = page ?? 1; //if (page == null) page = 1;

        // 5.3. Tạo kích thước trang (pageSize), mặc định là 5.
        int pageSize = (size ?? 5);

        ViewBag.pageSize = pageSize;

        // 6. Toán tử ?? trong C# mô tả nếu page khác null thì lấy giá trị page, còn
        // nếu page = null thì lấy giá trị 1 cho biến pageNumber. --- dammio.com
        int pageNumber = (page ?? 1);

        // 6.2 Lấy tổng số record chia cho kích thước để biết bao nhiêu trang
        int checkTotal = (StoreList.ToList().Count / pageSize) + 1;
        // Nếu trang vượt qua tổng số trang thì thiết lập là 1 hoặc tổng số trang
        if (pageNumber > checkTotal) pageNumber = checkTotal;

        // 7. Trả về các Link được phân trang theo kích thước và số trang.
        return View(StoreList.ToPagedList(pageNumber, pageSize));
    }
}

}