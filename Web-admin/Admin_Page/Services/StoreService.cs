﻿using Admin_Page.Environment;
using Admin_Page.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Admin_Page.Services
{
    public class StoreService
    {
        public StoreService()
        {
        }

        public  async Task<List<StoreViewModel>> GetStoreListAsync()
        {
            List<StoreViewModel> StoreList = new List<StoreViewModel>();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(ApiUrl.baseUrl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync(ApiUrl.GetListStores);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var StoreResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    StoreList = JsonConvert.DeserializeObject<List<StoreViewModel>>(StoreResponse);

                }
                //returning the employee list to view  
                return StoreList;
            }
        }
    }
}