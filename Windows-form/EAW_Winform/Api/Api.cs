﻿using EAW_Winform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EAW_Winform.Api
{
    public static class Api
    {
        static readonly HttpClient _client = new HttpClient();
        static Api()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(Properties.Resources.apiUrl);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static async Task<bool> SendImageAsync(ImageModel image)
        {
            var response = await _client.PostAsJsonAsync("Attendance", image);
            return response.IsSuccessStatusCode;
        }
    }
}
