﻿namespace EAW_Winform
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbCamera = new System.Windows.Forms.ComboBox();
            this.lblCamera = new System.Windows.Forms.Label();
            this.btnCapture = new System.Windows.Forms.Button();
            this.picBox = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBoxQrCode = new System.Windows.Forms.PictureBox();
            this.lblQRCode = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblResultCode = new System.Windows.Forms.Label();
            this.lbLastAttendance = new System.Windows.Forms.Label();
            this.lbConnectionStatus = new System.Windows.Forms.Label();
            this.lbConnectionStatusResult = new System.Windows.Forms.Label();
            this.lbDateTime = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQrCode)).BeginInit();
            this.SuspendLayout();
            // 
            // cbCamera
            // 
            this.cbCamera.FormattingEnabled = true;
            this.cbCamera.Location = new System.Drawing.Point(121, 73);
            this.cbCamera.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbCamera.Name = "cbCamera";
            this.cbCamera.Size = new System.Drawing.Size(395, 24);
            this.cbCamera.TabIndex = 6;
            // 
            // lblCamera
            // 
            this.lblCamera.AutoSize = true;
            this.lblCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamera.Location = new System.Drawing.Point(16, 74);
            this.lblCamera.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCamera.Name = "lblCamera";
            this.lblCamera.Size = new System.Drawing.Size(80, 20);
            this.lblCamera.TabIndex = 7;
            this.lblCamera.Text = "Camera:";
            // 
            // btnCapture
            // 
            this.btnCapture.Location = new System.Drawing.Point(380, 105);
            this.btnCapture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(137, 30);
            this.btnCapture.TabIndex = 11;
            this.btnCapture.Text = "Bắt đầu";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // picBox
            // 
            this.picBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picBox.Cursor = System.Windows.Forms.Cursors.No;
            this.picBox.ImageLocation = "";
            this.picBox.Location = new System.Drawing.Point(524, 15);
            this.picBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(1384, 932);
            this.picBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox.TabIndex = 12;
            this.picBox.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.No;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(16, 668);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 278);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBoxQrCode
            // 
            this.pictureBoxQrCode.Location = new System.Drawing.Point(17, 140);
            this.pictureBoxQrCode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxQrCode.Name = "pictureBoxQrCode";
            this.pictureBoxQrCode.Size = new System.Drawing.Size(500, 500);
            this.pictureBoxQrCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxQrCode.TabIndex = 14;
            this.pictureBoxQrCode.TabStop = false;
            // 
            // lblQRCode
            // 
            this.lblQRCode.AutoSize = true;
            this.lblQRCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQRCode.Location = new System.Drawing.Point(13, 116);
            this.lblQRCode.Name = "lblQRCode";
            this.lblQRCode.Size = new System.Drawing.Size(131, 20);
            this.lblQRCode.TabIndex = 16;
            this.lblQRCode.Text = "Quét QR Code";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(12, 644);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(147, 20);
            this.lblResult.TabIndex = 17;
            this.lblResult.Text = "Kết quả: Mã NV:";
            // 
            // lblResultCode
            // 
            this.lblResultCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultCode.Location = new System.Drawing.Point(187, 646);
            this.lblResultCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResultCode.Name = "lblResultCode";
            this.lblResultCode.Size = new System.Drawing.Size(328, 16);
            this.lblResultCode.TabIndex = 18;
            // 
            // lbLastAttendance
            // 
            this.lbLastAttendance.AutoSize = true;
            this.lbLastAttendance.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbLastAttendance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastAttendance.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lbLastAttendance.Location = new System.Drawing.Point(0, 970);
            this.lbLastAttendance.Name = "lbLastAttendance";
            this.lbLastAttendance.Size = new System.Drawing.Size(179, 20);
            this.lbLastAttendance.TabIndex = 19;
            this.lbLastAttendance.Text = "Lần cuối điểm danh:";
            // 
            // lbConnectionStatus
            // 
            this.lbConnectionStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbConnectionStatus.AutoSize = true;
            this.lbConnectionStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConnectionStatus.Location = new System.Drawing.Point(1325, 969);
            this.lbConnectionStatus.Name = "lbConnectionStatus";
            this.lbConnectionStatus.Size = new System.Drawing.Size(213, 20);
            this.lbConnectionStatus.TabIndex = 20;
            this.lbConnectionStatus.Text = "Trạng thái kết nối mạng:";
            // 
            // lbConnectionStatusResult
            // 
            this.lbConnectionStatusResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbConnectionStatusResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lbConnectionStatusResult.Location = new System.Drawing.Point(1580, 969);
            this.lbConnectionStatusResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbConnectionStatusResult.Name = "lbConnectionStatusResult";
            this.lbConnectionStatusResult.Size = new System.Drawing.Size(328, 21);
            this.lbConnectionStatusResult.TabIndex = 21;
            this.lbConnectionStatusResult.Text = "Trạng thái";
            // 
            // lbDateTime
            // 
            this.lbDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDateTime.Location = new System.Drawing.Point(15, 11);
            this.lbDateTime.Name = "lbDateTime";
            this.lbDateTime.Size = new System.Drawing.Size(500, 39);
            this.lbDateTime.TabIndex = 22;
            this.lbDateTime.Text = "Thời gian";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 990);
            this.Controls.Add(this.lbDateTime);
            this.Controls.Add(this.lbConnectionStatusResult);
            this.Controls.Add(this.lbConnectionStatus);
            this.Controls.Add(this.lbLastAttendance);
            this.Controls.Add(this.lblResultCode);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblQRCode);
            this.Controls.Add(this.pictureBoxQrCode);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picBox);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.lblCamera);
            this.Controls.Add(this.cbCamera);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EAW APP";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQrCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbCamera;
        private System.Windows.Forms.Label lblCamera;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.PictureBox picBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBoxQrCode;
        private System.Windows.Forms.Label lblQRCode;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblResultCode;
        private System.Windows.Forms.Label lbLastAttendance;
        private System.Windows.Forms.Label lbConnectionStatus;
        private System.Windows.Forms.Label lbConnectionStatusResult;
        private System.Windows.Forms.Label lbDateTime;
        private System.Windows.Forms.Timer timer2;
    }
}

