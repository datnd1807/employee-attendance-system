﻿using AForge.Video;
using AForge.Video.DirectShow;
using EAW_Winform.Model;
using EAW_Winform.utils;
using Emgu.CV;
using Emgu.CV.Dnn;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Spire.Barcode;
using StackExchange.Redis;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EAW_Winform
{
    public partial class MainForm : Form
    {

        private Directories directories = new Directories();
        //
        private FilterInfoCollection filter;
        private VideoCaptureDevice videoDevice;
        //redis
        private static string _ipAddress = "Localhost";
        private static int dbnumber = 0;
        private ConnectionMultiplexer _connection;
        private IDatabase _redisCache;
        // Aforge and emgu
        ImageConverter converter = new ImageConverter();
        //private Net _faceNet;
        private BackgroundWorker backgroundWorker;
        private int imageCount = 0;
        private Process recogProcess;
        // Winform
        private static string CREATEMODE = "1";
        private static string MACHINECODE = "M001";
        private static int TLLTIMEOUT = 5;
        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            InitConnection();
            getListCamera();
            CheckForInternetConnection();
            timer1.Start();
            InitQrCode();

            RunRecogProcess();
            backgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            backgroundWorker.DoWork += new DoWorkEventHandler(BackgroundWorker_DoWork);
            backgroundWorker.ProgressChanged += BackgroundWorkerOnProgressChangedAsync;
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BackgroundWorker_RunWorkerCompleted);
            backgroundWorker.RunWorkerAsync();

        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (videoDevice != null)
                {
                    if (videoDevice.IsRunning)
                    {
                        videoDevice.Stop();
                    }
                    if (_connection != null)
                    {
                        _connection.Close();
                    }
                    if (backgroundWorker.IsBusy)
                    {
                        backgroundWorker.Dispose();
                    }
                    if (!recogProcess.HasExited)
                    {
                        recogProcess.Kill();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception was thrown:" + ex.Message);
            }
        }
        private async void BackgroundWorkerOnProgressChangedAsync(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                object userObject = e.UserState;
                if (userObject != null)
                {
                    var result = (string)userObject;

                    dynamic json = JsonConvert.DeserializeObject(result);
                    JObject imageObjJson = json;
                    var imageObj = imageObjJson.ToObject<ImageModel>();
                    if (imageObj == null)
                    {
                        return;
                    }
                    imageObj.mode = CREATEMODE;
                    imageObj.createTime = DateTime.UtcNow.AddHours(7);
                    imageObj.faceMachineCode = MACHINECODE;
                    byte[] byteArray = Convert.FromBase64String(imageObj.base64StringImg);
                    MemoryStream stream = new MemoryStream(byteArray);
                    Image imageFromStream = Image.FromStream(stream);
                    pictureBox1.Image = imageFromStream;

                    lblResultCode.Text = imageObj.empCode;

                    if (imageObj.empCode != null)
                    {
                        await this.SendImageAsync(imageObj);
                    }
                    _redisCache.KeyDelete("image");
                    _redisCache.KeyDelete("employee");

                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            while (!worker.CancellationPending)
            {
                Thread.Sleep(500);
                if (_redisCache != null)
                {
                    try
                    {
                        var result = _redisCache.StringGet("employee").ToString();
                        worker.ReportProgress(0, result);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception was thrown:" + ex.Message);
                    }
                }
            }
            e.Result = "Background worker is canceled";
        }
        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //textBox1.AppendText(e.Result.ToString());
        }

        private void getListCamera()
        {
            filter = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (filter.Count != 0)
            {
                foreach (FilterInfo device in filter)
                {
                    cbCamera.Items.Add(device.Name);
                }
            }
            else
            {
                MessageBox.Show("Không tìm thấy webcam", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            cbCamera.SelectedIndex = 0;

        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            
            if(videoDevice != null)
            {
                videoDevice.Stop();
                videoDevice = null;
            }
            videoDevice = new VideoCaptureDevice(filter[cbCamera.SelectedIndex].MonikerString);
            videoDevice.NewFrame += Device_NewFrame;
            videoDevice.Start();
        }

        private void Device_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap bitmap = eventArgs.Frame.Clone() as Bitmap;
            if (imageCount >= 25)
            {

                Image<Rgb, byte> image = bitmap.ToImage<Rgb, byte>();
                var resizedImage = image.Resize(750, 421, Emgu.CV.CvEnum.Inter.Linear);
                //Image<Bgr, byte> image = bitmap.ToImage<Bgr, byte>();
                //_faceNet = DnnInvoke.ReadNetFromCaffe("deploy.prototxt", "res10_300x300_ssd_iter_140000.caffemodel");
                //Mat blob = DnnInvoke.BlobFromImage(image, 1, new Size(128, 128));
                //_faceNet.SetInput(blob);
                //var detection = _faceNet.Forward();
                //var array = detection.GetData();
                //for (int i = 0; i < detection.SizeOfDimension[2]; i++)
                //{
                //    var confidence = (float)array.GetValue(0, 0, i, 2);
                //    if (confidence > 0.8)
                //    {
                //        ////var idx = (int)p.At(i, 1);
                //        //var w1 = (int)((float)bitmap.Width * Convert.ToSingle(array.GetValue(0, 0, i, 3)));
                //        //var h1 = (int)((float)bitmap.Height * Convert.ToSingle(array.GetValue(0, 0, i, 4)));
                //        //var w2 = (int)((float)bitmap.Width * Convert.ToSingle(array.GetValue(0, 0, i, 5)));
                //        //var h2 = (int)((float)bitmap.Height * Convert.ToSingle(array.GetValue(0, 0, i, 6)));
                //        //Rectangle rectangle = new Rectangle(w1, h1, w2 - w1, h2 - h1);

                //        //using (Graphics graphics = Graphics.FromImage(bitmap))
                //        //{
                //        //    using (Pen pen = new Pen(Color.Red, 2))
                //        //    {
                //        //        graphics.DrawRectangle(pen, rectangle);
                //        //    }
                //        //}


                //    }
                //}
                SendToRecognize(resizedImage);
                imageCount = 0;
            }
            else
            {
                imageCount++;
            }
            try
            {
                picBox.Image = bitmap;
            }catch (Exception e)
            {

            }
        }

        private async Task<bool> SendImageAsync(ImageModel imageObj)
        {

            //Console.WriteLine(imageObj.empCode + ": " + await _redisCache.HashExistsAsync("checkedimage", new RedisValue(imageObj.empCode)));
            if (_redisCache.HashExists("checkedimage", new RedisValue(imageObj.empCode)))
            {
                return false;
            }
            else
            {
                await _redisCache.HashSetAsync("checkedimage", new RedisValue(imageObj.empCode), new RedisValue(imageObj.ToString()));
                await _redisCache.KeyExpireAsync("checkedimage", TimeSpan.FromMinutes(TLLTIMEOUT));
            }
            try
            {
                if (!(await Api.Api.SendImageAsync(imageObj)))
                {
                    _redisCache.HashDelete("checkedimage", new RedisValue(imageObj.empCode));
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("server error:" + ex.Message);
                _redisCache.HashDelete("checkedimage", new RedisValue(imageObj.empCode));
                return false;
            }
            lbLastAttendance.Text = "Lần cuối điểm danh: " + imageObj.empCode + " vào lúc: " + imageObj.createTime;
            return true;
        }
        private void SendToRecognize(Image<Rgb, byte> image)
        {
            Bitmap bitmap = image.AsBitmap();

            byte[] byteArray = (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

            string convertToArray = Convert.ToBase64String(byteArray);


            _redisCache.StringSet("image", convertToArray);
        }
        private void RunRecogProcess()
        {
            if (recogProcess == null)
            {
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = directories.userDirectory;

                start.Arguments = string.Format("{0} {1}", directories.faceRecognize, directories.imageOutput);
                start.UseShellExecute = false;
                start.RedirectStandardOutput = true;
                start.RedirectStandardError = true;
                start.CreateNoWindow = true;
                recogProcess = Process.Start(start);
            }


        }
        private void InitConnection()
        {
            DialogResult dialogResult;
            do
            {
                dialogResult = DialogResult.OK;
                try
                {
                    //ConfigurationOptions connectionConfig = new ConfigurationOptions()
                    //{
                    //    EndPoints = { _ipAddress+":"+port },
                    //    Ssl = true, 
                    //    ClientName = "db0",
                    //};
                    _connection = ConnectionMultiplexer.Connect(_ipAddress);
                    _redisCache = _connection.GetDatabase(dbnumber);
                    //textBox1.AppendText("Connected to redis");
                    _redisCache.KeyDelete("employee");
                    _redisCache.KeyDelete("image");
                    _redisCache.KeyDelete("checkedimage");
                    _redisCache.KeyDelete("image_output");
                }
                catch (RedisConnectionException conEx)
                {
                    dialogResult = MessageBox.Show("Unable to connect to redis:" + conEx.Message, "Redis Error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                }
                if (dialogResult.Equals(DialogResult.Cancel))
                {
                    this.Close();
                }
            } while (dialogResult.Equals(DialogResult.Retry));

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            QrCodeModel model = new QrCodeModel()
            {
                faceMachineCode = "M001",
                createTime = DateTime.Now
            };
            var image = this.GenerateQrCode(model);
            pictureBoxQrCode.Image = image;
            CheckForInternetConnection();
        }
        private Image GenerateQrCode(QrCodeModel model)
        {
            BarcodeSettings.ApplyKey("your key");
            BarcodeSettings settings = new BarcodeSettings();
            settings.Type = BarCodeType.QRCode;
            settings.ShowText = false;
            settings.AutoResize = true;
            settings.X = 2.0f;
            settings.QRCodeECL = QRCodeECL.H;
            //input data  
            string data = JsonConvert.SerializeObject(model);
            settings.Data = data;
            //generate QR code  
            BarCodeGenerator generator = new BarCodeGenerator(settings);
            Image QRbarcode = generator.GenerateImage();
            //display QR code image in picture box  
            return QRbarcode;
        }
        private void InitQrCode()
        {
            QrCodeModel model = new QrCodeModel()
            {
                faceMachineCode = MACHINECODE,
                createTime = DateTime.Now
            };
            var image = this.GenerateQrCode(model);
            pictureBoxQrCode.Image = image;
        }

        private void CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    lbConnectionStatusResult.ForeColor = Color.Blue;
                    lbConnectionStatusResult.Text = "Đã kết nối";
                }
            }
            catch
            {
                lbConnectionStatusResult.ForeColor = Color.Red;
                lbConnectionStatusResult.Text = "Mất kết nối";
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            var time = DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss", culture);
            lbDateTime.Text = time;
        }
    }
}
