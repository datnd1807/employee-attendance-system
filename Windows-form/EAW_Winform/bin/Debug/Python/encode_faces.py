# USAGE
# python encode_faces.py --dataset dataset --encodings encodings.pickle

# import the necessary packages
from imutils import paths
import face_recognition
import pickle
import cv2
import os
import sys

# datasetPath = sys.argv[1]
# encodingFile = sys.argv[2]
# detection_method = "cnn"
datasetPath = 'dataset'
encodingFile = 'encodings.pickle'
detection_method = "cnn"

# grab the paths to the input images in our dataset
print("[INFO] quantifying faces...")
imagePaths = list(paths.list_images(datasetPath))

# initialize the list of known encodings and known names
knownEncodings = []
knownIds = []

# loop over the image paths
for (i, imagePath) in enumerate(imagePaths):
	# extract the person name from the image path
	print("[INFO] processing image {}/{}".format(i + 1,
		len(imagePaths)))
	id = imagePath.split(os.path.sep)[-2]

	# load the input image and convert it from RGB (OpenCV ordering)
	# to dlib ordering (RGB)
	image = cv2.imread(imagePath)
	rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	# detect the (x, y)-coordinates of the bounding boxes
	# corresponding to each face in the input image
	boxes = face_recognition.face_locations(rgb,
		model=detection_method)

	# compute the facial embedding for the face
	encodings = face_recognition.face_encodings(rgb, boxes)

	# loop over the encodings
	for encoding in encodings:
		# add each encoding + id to our set of known ids and
		# encodings
		knownEncodings.append(encoding)
		knownIds.append(id)

# dump the facial encodings + names to disk
print("[INFO] serializing encodings...")
data = {"encodings": knownEncodings, "ids": knownIds}
f = open(encodingFile, "wb")
f.write(pickle.dumps(data))
f.close()