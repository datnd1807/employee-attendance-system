# USAGE
# python recognize_faces_image.py --encodings encodings.pickle --image examples/example_01.png --detection-method hog

# import the necessary packages
import face_recognition
import pickle
import cv2
import sys
import redis
import base64
import numpy as np
import json
import imutils
import time

# connect to redis server
re = redis.StrictRedis(host='localhost', port=6379, db=0)

# load arguments
encoding_file = sys.argv[1]
# encoding_file = "encodings.pickle"
detection_method = "cnn"

# load the known faces and embeddings
print("[INFO] loading encodings...")
data = pickle.loads(open(encoding_file, "rb").read())
padding = 100
while True:
	# load base64 string from redis
	img_b64 = re.get('image')
	if img_b64:
		# decode base64 string to bytes array
		img_bytes =  base64.b64decode(img_b64)
		# convert bytes array to one-dim numpy array
		img_arr = np.frombuffer(img_bytes, dtype=np.uint8) # im_arr is one-dim Numpy array

		# load the input image and convert it from BGR to RGB
		image = cv2.imdecode(img_arr, cv2.IMREAD_COLOR)
		# rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

		# rgb = imutils.resize(rgb, width=750)
		# r = image.shape[1] / float(rgb.shape[1])

		r = image.shape[1] /image.shape[1]	

		# detect the (x, y)-coordinates of the bounding boxes corresponding
		# to each face in the input image, then compute the facial embeddings
		# for each face
		boxes = face_recognition.face_locations(image,
												model=detection_method)
		encodings = face_recognition.face_encodings(image, boxes)

		# initialize the list of ids for each face detected
		ids = []
		percents= []
		# loop over the facial embeddings
		for encoding in encodings:
			# attempt to match each face in the input image to our known
			# encodings
			matches = face_recognition.compare_faces(data["encodings"],
													encoding, 0.4)
			id = "Unknown"
			percent = 0
			# check to see if we have found a match
			if True in matches:
				# find the indexes of all matched faces then initialize a
				# dictionary to count the total number of times each face
				# was matched
				matchedIdxs = [i for (i, b) in enumerate(matches) if b]
				counts = {}

				# loop over the matched indexes and maintain a count for
				# each recognized face face
				for i in matchedIdxs:
					id = data["ids"][i]
					counts[id] = counts.get(id, 0) + 1

				# determine the recognized face with the largest number of
				# votes (note: in the event of an unlikely tie Python will
				# select first entry in the dictionary)
				id = max(counts, key=counts.get)
				# compute accuracy of face
				percent = counts.get(id) / len(matchedIdxs) * 100
				if (id == "Unknown"):
					percent = 0

			# update the list of names

			ids.append(id)
			percents.append(percent)

		# loop over the recognized faces
		for ((top, right, bottom, left), id, percent) in zip(boxes, ids, percents):
			# draw the predicted face name on the image
			text = None
			em_id = None
			em_code = None
			if (id == "Unknown"):
				em_id = None
				em_code = "Unknown"
			else:
				id_arr = id.split("_")
				em_id = id_arr[0]
				em_code = id_arr[1]

			text = "{}: {:.2f}%".format(em_code, percent)

			top = int(top * r)
			right = int(right * r)
			bottom = int(bottom * r)
			left = int(left * r)

			# cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
			y = top - 5 if top - 5 > 5 else top + 5
			cv2.putText(image, text, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
						0.75, (0, 255, 0), 2)

			top = top - padding
			left = left - padding
			bottom = bottom + padding
			right = right + padding

			crop_img = image[top:bottom, left:right]

			if crop_img.any():
				# set result to redis
				crop_img = imutils.resize(crop_img, width=256)
				is_success, img_arr = cv2.imencode(".jpg", crop_img)  # im_arr: image in Numpy one-dim array format.
				img_bytes = img_arr.tobytes()
				img_b64 = base64.b64encode(img_bytes)
				employee = {
					"empCode": em_id,
					"base64StringImg": img_b64.decode('utf-8')
				}
				re.set("employee", json.dumps(employee))
				time.sleep(0.5)



	# re.set("image_output", img_b64.decode('utf-8'))

