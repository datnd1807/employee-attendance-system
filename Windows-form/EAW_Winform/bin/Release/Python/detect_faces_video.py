# USAGE
# python detect_faces_video.py --prototxt face_detection_model/deploy.prototxt --model face_detection_model/res10_300x300_ssd_iter_140000.caffemodel

# import the necessary packages
from imutils.video import VideoStream
import numpy as np
import shutil
import imutils
import time
import cv2
import os
import sys

prototxt = sys.argv[1]
model = sys.argv[2]
dataset_path = sys.argv[3]
# For each person, enter one numeric face id (must enter number start from 1, this is the lable of person 1)
face_id = sys.argv[4]
# Pass camera option 
cam_option = sys.argv[5]
# Create count and Padding and path
count = 0
PADDING = 30
# set min confidence ratio
confidenceMin = 0.5
# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(prototxt, model)

# create output folder
if os.path.exists(dataset_path + face_id):
    shutil.rmtree(dataset_path + face_id)
print("[INFO] creating folder with id: " + face_id)
os.mkdir(dataset_path + face_id)

# initialize the video stream and allow the cammera sensor to warmup
print("[INFO] starting video stream...")
vs = VideoStream(src=int(cam_option)).start()
time.sleep(2.0)

# loop over the frames from the video stream
while True:
    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 400 pixels
    frame = vs.read()
    frame = imutils.resize(frame, width=400)

    # grab the frame dimensions and convert it to a blob
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    # pass the blob through the network and obtain the detections and
    # predictions
    net.setInput(blob)
    detections = net.forward()

    # loop over the detections
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < confidenceMin:
            continue

        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")

        # draw the bounding box of the face along with the associated
        # probability
        startX = startX - PADDING
        startY = startY - PADDING
        endX = endX + PADDING
        endY = endY + PADDING

        y = startY - 10 if startY - 10 > 10 else startY + 10
        cv2.rectangle(frame, (startX, startY), (endX, endY),
                      (0, 0, 255), 1)
        # write image output to folder
        cv2.imwrite(dataset_path + face_id + "\\" + face_id + '.' + str(count + 1) + ".jpg",
                    frame[startY:endY, startX:endX])
        count = count + 1
    if count == 200:
        break

    # show the output frame
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

# do a bit of cleanup
cv2.destroyAllWindows()
res = "Image Save for ID: " + face_id
print("[INFO] " + res)
vs.stop()
