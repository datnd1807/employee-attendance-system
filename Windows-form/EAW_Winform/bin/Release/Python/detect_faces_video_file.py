import cv2
import os
from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
from imutils import paths
import imutils
import dlib
import sys

dataset = sys.argv[1]
landmark = sys.argv[2]
input_movie = sys.argv[3]
input_name = sys.argv[4]

frame_number = 0

datasetPath = dataset + "\\" + input_name

if os.path.isdir(datasetPath):
    frame_number = len(list(paths.list_images(datasetPath)))

else:
    os.mkdir(datasetPath)

movie = cv2.VideoCapture(input_movie)
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(landmark)
fa = FaceAligner(predictor)

padding = 0
while True:
    ret, frame = movie.read()
    if not ret:
        break
    print(frame_number)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 2)
    # loop over the face detections
    for rect in rects:
        # extract the ROI of the *original* face, then align the face
        # using facial landmarks
        (x, y, w, h) = rect_to_bb(rect)
        faceOrig = imutils.resize(frame[y:y + h, x:x + w])
        faceAligned = fa.align(frame, gray, rect)

        cv2.imwrite(datasetPath + "\\" + input_name + "." + str(frame_number) + ".png", faceAligned)
    frame_number += 1
    # Quit when the input video file ends
print("Extract completed")
