﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAW_Winform.utils
{
    public class Directories
    {
        public readonly string userDirectory;
        public readonly string datasetPath;
        public readonly string captureFace;
        public readonly string extractFaces;
        public readonly string faceRecognize;
        public readonly string imageOutput;
        public readonly string faceLandMark;
        public readonly string currentDir;
        public Directories()
        {
            currentDir = Directory.GetCurrentDirectory();
            userDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\AppData\\Local\\Programs\\Python\\Python38\\python.exe";
            datasetPath = currentDir + Properties.Resources.datasetPath;
            captureFace = currentDir + Properties.Resources.captureFace;
            extractFaces = currentDir + Properties.Resources.extractFaces;
            faceRecognize = currentDir + Properties.Resources.faceRecognizeImage;
            imageOutput = currentDir + Properties.Resources.imageOutput;
            faceLandMark = currentDir + Properties.Resources.faceLandMark;
        }

    }
}
